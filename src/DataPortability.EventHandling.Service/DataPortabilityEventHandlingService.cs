﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EventSourcing.EventHandling;
using EventSourcing.EventHandling.Autofac;

namespace DataPortability.EventHandling.Service
{
	public partial class DataPortabilityEventHandlingService : ServiceBase
	{
		public DataPortabilityEventHandlingService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			var builder = new ContainerBuilder();
			builder
				.RegisterType<EventProcessor>()
				.AsSelf();

			builder
				.RegisterModule(new EventHandlerModule(typeof()))
		}

		protected override void OnStop()
		{
		}
	}
}
