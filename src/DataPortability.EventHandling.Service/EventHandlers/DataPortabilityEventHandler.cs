﻿using System;
using System.Net.Http;
using DataPortability.EventHandling.Service.Events;
using EventSourcing.EventHandling;

namespace DataPortability.EventHandling.Service.EventHandlers
{
	public class DataPortabilityEventHandler
		: IEventHandler<QbizDataCollectedEvent>,
			IEventHandler<QboxDataCollectedEvent>,
			IEventHandler<EcedoDataCollectedEvent>,
			IEventHandler<ConsentDataCollectedEvent>
	{
		private readonly IRepository<DataCollectionSaga> _repository;

		public DataPortabilityEventHandler(IRepository<DataCollectionSaga> repository)
		{
			_repository = repository;
		}

		public void Handle(QbizDataCollectedEvent anEvent)
		{
			throw new System.NotImplementedException();
		}

		public void Handle(QboxDataCollectedEvent anEvent)
		{
			throw new System.NotImplementedException();
		}

		public void Handle(EcedoDataCollectedEvent anEvent)
		{
			throw new System.NotImplementedException();
		}

		public void Handle(ConsentDataCollectedEvent anEvent)
		{
			throw new System.NotImplementedException();
		}

		public void Handle(object anEvent)
		{
			if (anEvent is QbizDataCollectedEvent)
			{
				Handle((QbizDataCollectedEvent)anEvent);
			}

			if (anEvent is QboxDataCollectedEvent)
			{
				Handle((QboxDataCollectedEvent)anEvent);
			}

			if (anEvent is ConsentDataCollectedEvent)
			{
				Handle((ConsentDataCollectedEvent)anEvent);
			}

			if (anEvent is EcedoDataCollectedEvent)
			{
				Handle((EcedoDataCollectedEvent)anEvent);
			}
		}
	}

	public class DataCollectionSaga
		: Saga
	{
		public DataCollectionSaga(Guid id, ICommandPublisher commandPublisher) : base(id, commandPublisher)
		{
		}
	}

	public interface IRepository<TSaga> where TSaga : Saga
	{
		TSaga Get(Guid id);

		void Save(TSaga entity);
	}
}