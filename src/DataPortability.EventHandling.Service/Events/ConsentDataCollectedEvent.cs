﻿using System;
using EventSourcing.EventHandling;

namespace DataPortability.EventHandling.Service.Events
{
	public class ConsentDataCollectedEvent : IEvent
	{
		public Guid Id { get; set;}
	}
}