﻿using System;
using EventSourcing.EventHandling;

namespace DataPortability.EventHandling.Service.Events
{
	public class EcedoDataCollectedEvent : IEvent
	{
		public Guid Id { get; set; }
	}
}