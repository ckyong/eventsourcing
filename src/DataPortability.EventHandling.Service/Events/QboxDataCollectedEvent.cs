﻿using System;
using EventSourcing.EventHandling;

namespace DataPortability.EventHandling.Service.Events
{
	public class QboxDataCollectedEvent : IEvent
	{
		public Guid Id { get; set; }
	}
}