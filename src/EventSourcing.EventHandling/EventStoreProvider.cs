﻿using System;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace EventSourcing.EventHandling
{
	public class EventStoreProvider
	{
		private readonly IEventStoreConnection _eventStoreConnection;

		public EventStoreProvider(IEventStoreConnection eventStoreConnection)
		{
			_eventStoreConnection = eventStoreConnection;
		}

		public void SubscribeToAllFromBeginning(Func<IEvent, Task> handleEvent)
		{
			_eventStoreConnection.SubscribeToAllFrom(null, CatchUpSubscriptionSettings.Default, (subscription, @event) => EventAppeared(@event.OriginalEvent.Data, @event.Event.EventType, handleEvent));
		}

		internal Task EventAppeared(byte[] eventData, string eventType, Func<IEvent, Task> handleEvent)
		{
			IEvent @event = EventConverter.GetEventFromData<IEvent>(eventData, eventType);
			return handleEvent(@event);
		}
	}
}