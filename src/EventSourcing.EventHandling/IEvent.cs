﻿using System;

namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Interface for implementing events.
	/// </summary>
	public interface IEvent
	{
		Guid Id { get; }
	}
}