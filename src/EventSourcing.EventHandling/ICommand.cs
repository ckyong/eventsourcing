﻿using System;

namespace EventSourcing.EventHandling
{
	public interface ICommand
	{
		Guid Id { get; }
	}
}