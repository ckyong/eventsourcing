﻿using System.Collections.Generic;

namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Publishes an <see cref="ICommand"/>
	/// </summary>
	public interface ICommandPublisher
	{
		/// <summary>
		/// Publishes the provided <paramref name="command"/> on the command bus.
		/// </summary>
		void Publish<TCommand>(TCommand command)
			where TCommand : ICommand;

		/// <summary>
		/// Publishes the provided <paramref name="commands"/> on the command bus.
		/// </summary>
		void Publish<TCommand>(IEnumerable<TCommand> commands)
			where TCommand : ICommand;
	}
}