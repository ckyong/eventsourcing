﻿using System;

namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Base class for creating Sagas, or Process Managers.
	/// </summary>
	public abstract class Saga
	{
		public Guid Id { get; }

		protected ICommandPublisher CommandPublisher { get; set; }

		protected Saga(Guid id, ICommandPublisher commandPublisher)
		{
			Id = id;
			CommandPublisher = commandPublisher;
		}
	}
}