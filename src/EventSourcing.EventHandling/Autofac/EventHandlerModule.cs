﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace EventSourcing.EventHandling.Autofac
{
	public class EventHandlerModule : Module
	{
		private readonly Type _eventHandlerType;

		/// <summary>
		/// Initializes a new instance of the <see cref="EventHandlerModule"/> class. Accepts a type of an event handler, scans the assembly of that type for any event handlers, then registers them.
		/// </summary>
		/// <param name="eventHandlerType">Type of the event handler.</param>
		public EventHandlerModule(Type eventHandlerType)
		{
			if (eventHandlerType == null)
			{
				throw new ArgumentNullException(nameof(eventHandlerType));
			}

			_eventHandlerType = eventHandlerType;
		}

		protected override void Load(ContainerBuilder builder)
		{
			var handlerTypes = Assembly.GetAssembly(_eventHandlerType).GetTypes()
				.Where(type => !type.IsAbstract && type.IsAssignableTo<IEventHandler>());

			foreach (var type in handlerTypes)
			{
				builder.RegisterType(type)
					.AsImplementedInterfaces()
					.SingleInstance();
			}

			base.Load(builder);
		}
	}
}