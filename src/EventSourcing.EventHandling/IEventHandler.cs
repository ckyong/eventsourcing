﻿namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Interface for implementing event handlers.
	/// </summary>
	/// <typeparam name="TEvent">The type of the event.</typeparam>
	/// <seealso cref="EventSourcing.EventHandling.IEventHandler" />
	public interface IEventHandler<in TEvent> : IEventHandler
	{
		/// <summary>
		/// Handles the specified event.
		/// </summary>
		/// <param name="anEvent">An event.</param>
		void Handle(TEvent anEvent);
	}

	/// <summary>
	/// Interface for implementing event handlers.
	/// </summary>
	/// <seealso cref="EventSourcing.EventHandling.IEventHandler" />
	public interface IEventHandler
	{
		/// <summary>
		/// Handles the specified event.
		/// </summary>
		/// <param name="anEvent">An event.</param>
		void Handle(object anEvent);
	}
}
