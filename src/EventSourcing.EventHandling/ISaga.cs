﻿namespace EventSourcing.EventHandling
{
	public interface ISaga<TEvent> where TEvent : IEvent
	{
		/// <summary>
		/// Handles the specified event, publishing any commands if needed. State changes to the saga should be executed by calling the <see cref="Apply"/> method.
		/// </summary>
		/// <param name="event">The event.</param>
		void Handle(TEvent @event);

		/// <summary>
		/// Applies the specified event onto the saga, updating the state only. Should be used for replaying an event.
		/// </summary>
		/// <param name="event">The event.</param>
		void Apply(TEvent @event);
	}
}