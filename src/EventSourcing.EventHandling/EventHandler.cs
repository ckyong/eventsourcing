﻿namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Base class for implementing event handlers. 
	/// </summary>
	/// <typeparam name="TEvent">The type of the event.</typeparam>
	/// <seealso cref="EventSourcing.EventHandling.IEventHandler{TEvent}" />
	public abstract class EventHandler<TEvent> : IEventHandler<TEvent>
	{
		/// <summary>
		/// Handles the specified event.
		/// </summary>
		/// <param name="anEvent">An event.</param>
		public abstract void Handle(TEvent anEvent);

		/// <summary>
		/// Handles the specified event by casting it to <typeparam name="TEvent">the type of the event</typeparam> and then calling the <see cref="Handle(TEvent)"/> method.
		/// </summary>
		/// <param name="anEvent">An event.</param>
		public void Handle(object anEvent)
		{
			Handle((TEvent) anEvent);
		}
	}
}