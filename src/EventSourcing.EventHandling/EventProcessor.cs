﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace EventSourcing.EventHandling
{
	/// <summary>
	/// Event processor that uses an Autofac component context to resolve a event handlers for a given event.
	/// </summary>
	public class EventProcessor
	{
		private readonly IComponentContext _context;
		private readonly Type[] _eventTypes;

		/// <summary>
		/// Initializes a new instance of the <see cref="EventProcessor"/> class. If any event types are specified, these will be used for determining whether this specific instance will only listen to the given event types. Otherwise, all event types will be accepted. In the latter case, all events that do not have a registered event handler will simply be ignored.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="eventTypes">The handler types.</param>
		/// <exception cref="ArgumentNullException">context</exception>
		public EventProcessor(IComponentContext context, params Type[] eventTypes)
		{
			_context = context;
			_eventTypes = eventTypes;
		}

		/// <summary>
		/// Resolves the event handlers for the specified event and executes it.
		/// </summary>
		/// <param name="anEvent">The event to handle.</param>
		public void Handle(IEvent anEvent)
		{
			if (_eventTypes.Any() && !_eventTypes.Contains(anEvent.GetType()))
			{
				return;
			}

			foreach (IEventHandler handler in GetHandlersFor(anEvent))
			{
				handler.Handle(anEvent);
			}
		}

		protected virtual IEnumerable<IEventHandler> GetHandlersFor(IEvent anEvent)
		{
			Type handlerType = typeof(IEventHandler<>).MakeGenericType(anEvent.GetType());
			Type handlerCollectionType = typeof(IEnumerable<>).MakeGenericType(handlerType);

			return _context.Resolve(handlerCollectionType) as IEnumerable<IEventHandler>;
		}
	}
}