﻿using System.Collections.Generic;
using Autofac;
using EventSourcing.EventHandling.Autofac;
using EventSourcing.EventHandling.Tests.TestClasses;
using FluentAssertions;
using NUnit.Framework;

namespace EventSourcing.EventHandling.Tests
{
	[TestFixture]
	internal class AutofacModuleTests
	{
		public class When_the_module_is_registered
		{
			private EventHandlerModule _eventHandlerModule;

			[SetUp]
			public void SetUp()
			{
				_eventHandlerModule = new EventHandlerModule(typeof(OrderPlacedEventHandler));
			}

			[Test]
			public void It_should_scan_the_assembly_for_event_handlers_and_register_them()
			{
				// Arrange
				var builder = new ContainerBuilder();

				// Act
				builder.RegisterModule(_eventHandlerModule);
				var container = builder.Build();

				// Assert
				container.IsRegistered<IEnumerable<IEventHandler<OrderProcessedEvent>>>().Should().BeTrue("because it should have registered all the event handlers as their interfaces.");
				container.IsRegistered<IEnumerable<IEventHandler<OrderPlacedEvent>>>().Should().BeTrue("because it should have registered all the event handlers as their interfaces.");
			}
		}
	}
}