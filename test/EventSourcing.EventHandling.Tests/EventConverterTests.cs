﻿using System;
using System.Text;
using EventSourcing.EventHandling.Tests.TestClasses;
using FluentAssertions;
using NUnit.Framework;

namespace EventSourcing.EventHandling.Tests
{
	[TestFixture]
	internal class EventConverterTests
	{
		public class When_deserializing_a_json_stream
		{
			[Test]
			public void It_should_deserialize_to_an_event()
			{
				// Arrange
				var eventId = Guid.NewGuid();
				var json = $"{{\"Id\":\"{eventId}\"}}";
				var eventType = typeof(OrderPlacedEvent).AssemblyQualifiedName;

				// Act
				var @event = EventConverter.GetEventFromData<IEvent>(Encoding.Default.GetBytes(json), eventType);

				// Assert
				@event.Should().BeOfType<OrderPlacedEvent>("because the event should have been properly deserialized.");
				@event.Id.Should().Be(eventId, "because the id should have been properly deserialized.");
			}

			[Test]
			public void It_should_ignore_fields_that_do_not_exist()
			{
				// Arrange
				var eventId = Guid.NewGuid();
				var json = $"{{\"Id\":\"{eventId}\", \"nonExistentId\":\"123456789\"}}";
				var eventType = typeof(OrderPlacedEvent).AssemblyQualifiedName;

				// Act
				var @event = EventConverter.GetEventFromData<IEvent>(Encoding.Default.GetBytes(json), eventType);

				// Assert
				@event.Should().BeOfType<OrderPlacedEvent>("because the non-defined fields should have been ignored.");
				@event.Id.Should().Be(eventId, "because the id should have been properly deserialized.");
			}

			[Test]
			public void It_should_return_null_if_the_type_does_not_exist()
			{
				// Arrange
				var eventId = Guid.NewGuid();
				var json = $"{{\"Id\":\"{eventId}\", \"nonExistentId\":\"123456789\"}}";
				var eventType = "bullshitType";

				// Act
				var @event = EventConverter.GetEventFromData<IEvent>(Encoding.Default.GetBytes(json), eventType);

				// Assert
				@event.Should().BeNull("because there is no type called \"bullshitType\"");
			}
		}
	}
}
