﻿using System;
using System.Linq;
using Autofac;
using EventSourcing.EventHandling.Tests.TestClasses;
using FluentAssertions;
using NUnit.Framework;

namespace EventSourcing.EventHandling.Tests
{
	[TestFixture]
	internal class EventHandlerProviderTests
	{
		private TestEventProcessor _resolver;
		private ILifetimeScope _lifetimeScope;

		[TearDown]
		public void TearDown()
		{
			_lifetimeScope.Dispose();
		}

		public class When_an_event_is_received : EventHandlerProviderTests
		{
			[SetUp]
			public void SetUp()
			{
				var builder = new ContainerBuilder();
				builder
					.RegisterType<OrderPlacedEventHandler>()
					.AsImplementedInterfaces()
					.SingleInstance();

				builder
					.RegisterType<OrderProcessedEventHandler>()
					.AsImplementedInterfaces()
					.SingleInstance();

				builder
					.RegisterType<SecondOrderProcessedHandler>()
					.AsImplementedInterfaces()
					.SingleInstance();

				var handlerTypes = new[]
				{
					typeof(OrderProcessedEvent),
					typeof(OrderPlacedEvent)
				};

				builder.RegisterType<TestEventProcessor>()
					.WithParameter(new TypedParameter(typeof(Type[]), handlerTypes))
					.AsSelf()
					.SingleInstance();

				_lifetimeScope = builder.Build().BeginLifetimeScope();
				_resolver = _lifetimeScope.Resolve<TestEventProcessor>();
			}

			[Test]
			public void It_should_resolve_a_handler_for_a_given_event()
			{
				// Arrange
				var @event = new OrderPlacedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.Single().Should().BeOfType<OrderPlacedEventHandler>("because the handler was properly registered.");
			}

			[Test]
			public void It_should_resolve_a_handler_for_other_given_events()
			{
				// Arrange
				var @event = new OrderProcessedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.First().Should().BeOfType<OrderProcessedEventHandler>("because these events were registered also.");
			}

			[Test]
			public void It_should_handle_the_event_using_the_handler()
			{
				// Arrange
				var @event = new OrderPlacedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.First().As<OrderPlacedEventHandler>().LastHandledEvent.Should().BeOfType<OrderPlacedEvent>("because it should use the resolved handler to handle the event.");
			}

			[Test]
			public void It_should_run_all_the_handlers_that_are_subscribed_to_the_given_event()
			{
				// Arrange
				var @event = new OrderProcessedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.Count().Should().Be(2);
				_resolver.ResolvedHandlers.Should().AllBeAssignableTo<IEventHandler<OrderProcessedEvent>>("because these handlers were all registered to listen to the event.");
			}

			[Test]
			public void It_should_handle_the_event_using_all_handlers()
			{
				// Arrange
				var @event = new OrderProcessedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.First().As<OrderProcessedEventHandler>().LastHandledEvent.Should().BeOfType<OrderProcessedEvent>("because it should have handled the event.");
				_resolver.ResolvedHandlers.Last().As<SecondOrderProcessedHandler>().LastHandledEvent.Should().BeOfType<OrderProcessedEvent>("because it should have handled the event also.");
			}

			[Test]
			public void It_should_ignore_events_that_the_resolver_was_not_registered_for()
			{
				// Arrange
				var @event = new AccountAddedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.Should().BeNull("because no resolution may occur if the handler type is not amongst the specified handlers.");
			}
		}

		public class When_no_handlertypes_were_given : EventHandlerProviderTests
		{
			[SetUp]
			public void SetUp()
			{
				var builder = new ContainerBuilder();

				builder.RegisterType<OrderPlacedEventHandler>()
					.AsImplementedInterfaces()
					.SingleInstance();

				builder.RegisterType<TestEventProcessor>()
					.AsSelf()
					.SingleInstance();

				_lifetimeScope = builder.Build().BeginLifetimeScope();
				_resolver = _lifetimeScope.Resolve<TestEventProcessor>();
			}

			[Test]
			public void It_should_ignore_unregistered_resolutions()
			{
				// Arrange
				var @event = new AccountAddedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.Should().BeEmpty("because resolution should have occurred, but no registered handlers should have been found.");
			}

			[Test]
			public void It_should_still_resolve_registered_handlers()
			{
				// Arrange
				var @event = new OrderPlacedEvent(Guid.NewGuid());

				// Act
				_resolver.Handle(@event);

				// Assert
				_resolver.ResolvedHandlers.Single().Should().BeOfType<OrderPlacedEventHandler>("because that specific handler was properly registered.");
			}
		}
	}
}