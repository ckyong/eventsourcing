﻿namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class OrderProcessedEventHandler : EventHandler<OrderProcessedEvent>
	{
		public IEvent LastHandledEvent { get; set; }

		public override void Handle(OrderProcessedEvent anEvent)
		{
			LastHandledEvent = anEvent;
		}
	}
}