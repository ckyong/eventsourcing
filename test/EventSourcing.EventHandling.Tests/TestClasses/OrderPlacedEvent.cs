﻿using System;

namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class OrderPlacedEvent : IEvent
	{
		public Guid Id { get; set; }

		public OrderPlacedEvent(Guid id)
		{
			Id = id;
		}
	}
}