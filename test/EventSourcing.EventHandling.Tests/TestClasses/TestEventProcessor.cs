﻿using System;
using System.Collections.Generic;
using Autofac;

namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class TestEventProcessor : EventProcessor
	{
		public IEnumerable<IEventHandler> ResolvedHandlers;

		public TestEventProcessor(IComponentContext context, Type[] eventTypes) : base(context, eventTypes)
		{
		}

		protected override IEnumerable<IEventHandler> GetHandlersFor(IEvent anEvent)
		{
			ResolvedHandlers = base.GetHandlersFor(anEvent);
			return ResolvedHandlers;
		}
	}
}