﻿using System;

namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class AccountAddedEvent : IEvent
	{
		public AccountAddedEvent(Guid id)
		{
			Id = id;
		}

		public Guid Id { get; }
	}
}