﻿using System;

namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class TestSaga : Saga, ISaga<OrderPlacedEvent>
	{
		public IEvent ProcessedEvent { get; set; }

		public TestSaga(Guid id, ICommandPublisher commandPublisher) : base(id, commandPublisher)
		{
		}

		public void Handle(OrderPlacedEvent @event)
		{
			Apply(@event);
			CommandPublisher.Publish(new ProcessOrderCommand(@event.Id));
		}

		public void Apply(OrderPlacedEvent @event)
		{
			ProcessedEvent = @event;
		}
	}
}