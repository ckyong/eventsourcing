﻿using System;

namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class OrderProcessedEvent : IEvent
	{
		public Guid Id { get; }

		public OrderProcessedEvent(Guid id)
		{
			Id = id;
		}
	}
}
