﻿namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class OrderPlacedEventHandler : EventHandler<OrderPlacedEvent>
	{
		public IEvent LastHandledEvent;

		public override void Handle(OrderPlacedEvent anEvent)
		{
			LastHandledEvent = anEvent;
		}
	}
}