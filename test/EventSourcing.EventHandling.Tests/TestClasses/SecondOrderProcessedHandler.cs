﻿namespace EventSourcing.EventHandling.Tests.TestClasses
{
	internal class SecondOrderProcessedHandler : IEventHandler<OrderProcessedEvent>
	{
		public IEvent LastHandledEvent { get; set; }

		public void Handle(OrderProcessedEvent anEvent)
		{
			LastHandledEvent = anEvent;
		}

		public void Handle(object anEvent)
		{
			Handle((OrderProcessedEvent) anEvent);
		}
	}
}