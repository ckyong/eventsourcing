﻿using System;

namespace EventSourcing.EventHandling.Tests
{
	internal class ProcessOrderCommand : ICommand
	{
		public ProcessOrderCommand(Guid id)
		{
			Id = id;
		}

		public Guid Id { get; }
	}
}