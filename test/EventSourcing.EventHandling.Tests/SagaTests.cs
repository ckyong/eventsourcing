﻿using System;
using EventSourcing.EventHandling.Tests.TestClasses;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EventSourcing.EventHandling.Tests
{
	[TestFixture]
	internal class SagaTests
	{
		private Mock<ICommandPublisher> _commandPublisherMock;
		private Guid _sagaId;
		private TestSaga _saga;

		[SetUp]
		public void SetUp()
		{
			_commandPublisherMock = new Mock<ICommandPublisher>();
			_sagaId = Guid.NewGuid();
			_saga = new TestSaga(_sagaId, _commandPublisherMock.Object);
		}

		public class When_handling_an_event : SagaTests
		{
			[Test]
			public void It_should_be_able_to_publish_a_command_if_needed()
			{
				// Arrange
				var eventId = Guid.NewGuid();
				var @event = new OrderPlacedEvent(eventId);

				// Act
				_saga.Handle(@event);

				// Assert
				_commandPublisherMock.Verify(m => m.Publish(It.IsAny<ICommand>()), Times.Once, "because it should apply the change and publish a command.");
				_saga.ProcessedEvent.Should().Be(@event, "because the event should have been applied.");
			}
		}

		public class When_replaying_an_event : SagaTests
		{
			[Test]
			public void It_should_only_apply_the_change()
			{
				// Arrange
				var eventId = Guid.NewGuid();
				var @event = new OrderPlacedEvent(eventId);

				// Act
				_saga.Apply(@event);

				// Assert
				_commandPublisherMock.Verify(m => m.Publish(It.IsAny<ICommand>()), Times.Never, "because it should apply the change without publishing anything.");
				_saga.ProcessedEvent.Should().Be(@event, "because the event should have been applied.");
			}
		}
	}
}