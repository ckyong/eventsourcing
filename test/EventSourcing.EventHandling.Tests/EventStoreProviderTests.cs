﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSourcing.EventHandling.Tests.TestClasses;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EventSourcing.EventHandling.Tests
{
	[TestFixture]
	internal class EventStoreProviderTests
	{
		private EventStoreProvider _provider;
		private Mock<IEventStoreConnection> _eventStoreConnectionMock;

		[SetUp]
		public void SetUp()
		{
			_eventStoreConnectionMock = new Mock<IEventStoreConnection>();
			_provider = new EventStoreProvider(_eventStoreConnectionMock.Object);
		}

		public class When_subscribing_to_all_streams : EventStoreProviderTests
		{
			private IEvent _eventHandled;

			[Test]
			public void It_should_subscribe_to_the_inner_event_store()
			{
				// Act
				_provider.SubscribeToAllFromBeginning(HandleEvent);

				// Assert
				_eventStoreConnectionMock.Verify(m=>m.SubscribeToAllFrom(null, CatchUpSubscriptionSettings.Default, It.IsAny<Func<EventStoreCatchUpSubscription, ResolvedEvent, Task>>(), null, null, null), Times.Once);		
			}

			[Test]
			public void It_should_deserialize_any_incoming_event_and_handle_it()
			{
				// Arrange
				var eventData = $"{{\"Id\":\"{Guid.NewGuid()}\"}}";
				var eventType = typeof(OrderPlacedEvent).AssemblyQualifiedName;

				// Act
				_provider.EventAppeared(Encoding.Default.GetBytes(eventData), eventType, HandleEvent);

				// Assert
				_eventHandled.Should().BeOfType<OrderPlacedEvent>();
			}

			private Task HandleEvent(IEvent incomingEvent)
			{
				_eventHandled = incomingEvent;
				return Task.CompletedTask;
			}
		}
	}
}
